# 世界上最厉害的网盘  V0.1
## 测试页面：https://035m038513.picp.vip/
#### 介绍
此项目为学习阶段构建

#### 近期计划

1. 添加页面css
2. 对数据进行分页展示
3. 添加JWT认证登录，为每个账号分配网盘空间，并可分享资源链接
4. 用户可以选择资源是否共享，共享的资源可在资源检索页面搜索到
5. 添加资源的 上传时间、下载次数、资源大小、资源类型、上传用户信息
6. 文件转存，将其他用户共享的文件存入自己的网盘
7. 构建多套GateWay，减少接口的暴露，解决跨域困难认证等问题


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


## 架构图

![架构图](https://images.gitee.com/uploads/images/2020/1028/162413_9b1ee84d_7799722.png "B69D4FD5-3EAF-4984-8570-B0557865D87A.png")