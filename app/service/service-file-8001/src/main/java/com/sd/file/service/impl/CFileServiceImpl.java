package com.sd.file.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sd.entity.Result;
import com.sd.entity.StatusCode;
import com.sd.file.dao.FileDao;
import com.sd.file.service.IFileService;
import com.sd.file.util.FileDown;
import com.sd.good.pojo.Album;
import com.sd.good.pojo.Brand;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

@Service
public class CFileServiceImpl implements IFileService {
    @Autowired
    private FileDao fileDao;

    /**
     *  模糊搜索模板
     * @param brand
     * @return
     */
    public Example Example(Brand brand) {
        Example example = new Example(Brand.class);
        Example.Criteria criteria = example.createCriteria();
        if (brand!=null)
            //如果名字不为空，则模糊搜索名字
            if (!StringUtil.isEmpty(brand.getName())){
                criteria.andLike("name","%"+brand.getName()+"%");
            }
        //如果首字母不为空，则搜索首字母
        if (!StringUtil.isEmpty(brand.getLetter())) {
            criteria.andEqualTo("letter", brand.getLetter());
        }
        return example;
    }

    @Override
    public List<Album> findByExample(Album album) {
        return null;
    }

    @Override
    public PageInfo<Album> findAllByPage(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        List<Album> albums = fileDao.selectAll();
        return new PageInfo<Album>(albums);
    }

    @Override
    public PageInfo<Album> findExampleByPage(Album album, Integer page, Integer size) {
        return null;
    }

    @Override
    public List<Album> findAll() {
        List<Album> albumList = fileDao.selectAll();
        return albumList;
    }

    @Override
    public Album findById(Integer id) {
        Album album = fileDao.selectByPrimaryKey(id);
        return album;
    }

    @Override
    public void insert(Album album) {
        fileDao.insert(album);
    }

    @Override
    public void update(Album album) {
        fileDao.updateByPrimaryKeySelective(album);
    }

    @Override
    public void delete(Integer id) {
        fileDao.deleteByPrimaryKey(id);

    }

    public Result deleteFile(String group,String filename) throws Exception {
        FileDown fileDown = new FileDown();
        fileDown.deleteFile(group, filename);
        return new Result(true, StatusCode.OK, "删除成功");
    }

    public static void main(String[] args) throws Exception {
        CFileServiceImpl cFileService = new CFileServiceImpl();
        cFileService.deleteFile("group1", "M00/00/00/rBkGKF-ZGtiAS9LvAAjhdSiEZbw56.docx");
    }
}
