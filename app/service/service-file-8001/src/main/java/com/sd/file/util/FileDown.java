package com.sd.file.util;

import com.sd.good.pojo.FastDFSFile;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.core.io.ClassPathResource;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileDown {

    private static TrackerClient trackerClient;
    private static TrackerServer connection;
    private static StorageClient storageClient;

    /**
     *   配置文件加载
     */
    static {
        try {
            String filepath = new ClassPathResource("fds_client.conf").getPath();
            ClientGlobal.init(filepath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 连接信息初始化
     * @throws IOException
     */
    static void init() throws IOException {
        trackerClient = new TrackerClient();
        connection = trackerClient.getConnection();
        storageClient = new StorageClient(connection, null);
    }

    /**
     * 文件上传
     * @param fastDFSFile
     * @return
     * @throws Exception
     */
    public String[] upload (FastDFSFile fastDFSFile) throws Exception {
        init();
        NameValuePair[] nameValuePairs = new NameValuePair[1];
        nameValuePairs[0] = new NameValuePair("autuor",fastDFSFile.getAutuor());
        String[] result = storageClient.upload_file(fastDFSFile.getContent(),fastDFSFile.getExt(),nameValuePairs);
        return result;
    }

    /**
     * 文件下载
     * @param group_name
     * @param remote_filename
     * @return
     * @throws Exception
     */
    public InputStream getFile(String group_name, String remote_filename) throws Exception{
        init();
        byte[] download_file = storageClient.download_file(group_name, remote_filename);
        return new ByteArrayInputStream(download_file);
    }

    /**
     * 文件删除
     * @param group_name
     * @param remote_filename
     * @throws Exception
     */
    public void deleteFile(String group_name, String remote_filename) throws Exception{
        init();
        storageClient.delete_file(group_name, remote_filename);
    }

    /**
     * 文件更新
     * @param fastDFSFile
     * @throws Exception
     * @return
     */
    public String[] updateFile(FastDFSFile fastDFSFile) throws Exception{
        init();
        String[] result =storageClient.upload_file(fastDFSFile.getContent(),fastDFSFile.getExt(),null);
        return result;
    }

}
