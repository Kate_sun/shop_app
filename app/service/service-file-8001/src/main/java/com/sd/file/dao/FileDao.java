package com.sd.file.dao;

import com.sd.good.pojo.Album;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface FileDao extends Mapper<Album> {

}
