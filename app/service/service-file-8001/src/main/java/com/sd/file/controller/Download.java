package com.sd.file.controller;

import com.sd.entity.Result;
import com.sd.file.service.IFileService;
import com.sd.file.service.impl.CFileServiceImpl;
import com.sd.good.pojo.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@CrossOrigin
public class Download {
    @Autowired
    private CFileServiceImpl fileService;
    @Autowired
    private FileController fileController;

    @GetMapping
    public ModelAndView downloadList(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        List<Album> all = fileService.findAll();
        modelAndView.setViewName("index");
        modelAndView.addObject("fileList", all);
        return modelAndView;
    }

    @PostMapping("/download")
    public ModelAndView upload(@RequestParam(value = "file") MultipartFile file) throws Exception {
        Result result = fileController.fileUpload(file);
        String data = (String) result.getData();
        Album album = new Album();
        album.setTitle(file.getOriginalFilename());
        album.setImage(data.substring(7));
        fileController.insertAlbum(album);
        ModelAndView modelAndView = new ModelAndView();
        List<Album> all = fileService.findAll();
        modelAndView.setViewName("index");
        modelAndView.addObject("fileList", all);
        return modelAndView;
    }

    @GetMapping("/{id}")
    public ModelAndView delete(@PathVariable(value = "id")Integer id) throws Exception {
        Album album = fileService.findById(id);
        fileService.delete(id);
        fileService.deleteFile("group1", album.getImage().substring(31));
        ModelAndView modelAndView = new ModelAndView();
        List<Album> all = fileService.findAll();
        modelAndView.setViewName("index");
        modelAndView.addObject("fileList", all);
        return modelAndView;
    }
}
