package com.sd.file.controller;


import com.github.pagehelper.PageInfo;
import com.sd.entity.Result;
import com.sd.entity.StatusCode;
import com.sd.file.service.impl.CFileServiceImpl;
import com.sd.file.util.FileDown;
import com.sd.good.pojo.Album;
import com.sd.good.pojo.FastDFSFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

@ResponseBody
@RequestMapping("/file")
@CrossOrigin
@Controller
public class FileController {

    @Autowired
    private CFileServiceImpl fileService;

    /**
     * 文件上传
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping
    public Result fileUpload(@RequestParam(value = "file")MultipartFile file) throws Exception {
        FastDFSFile fastDFSFile = new FastDFSFile(file.getOriginalFilename(),file.getBytes(), StringUtils.getFilenameExtension(file.getOriginalFilename()));
        FileDown fileDown = new FileDown();
        String[] result = fileDown.upload(fastDFSFile);
        String filepath = "http://http://182.92.8.23:8080/"+result[0]+"/"+result[1];
        return new Result(true, StatusCode.OK, "上传成功",filepath);
    }

    /**
     * 文件下载
     * @param group
     * @param filename
     * @return
     * @throws Exception
     */
    @PostMapping("/{group}")
    public Result fileDownLoad(@PathVariable(value = "group")String group,
                               @RequestParam(value = "filename")String filename) throws Exception {
        FileDown fileDown = new FileDown();
        InputStream download_file = fileDown.getFile(group,filename);
        FileOutputStream os = new FileOutputStream("1.jpg");
        byte[] buffer = new byte[1024];
        while (download_file.read(buffer)!=-1){
            os.write(buffer);
        }
        os.flush();
        os.close();
        return new Result(true, StatusCode.OK, "下载成功");
    }

    /**
     * 文件删除
     * @param group
     * @param filename
     * @return
     * @throws Exception
     */
    @DeleteMapping("{group}")
    public Result deleteFile(@PathVariable(value = "group")String group,
                               @RequestParam(value = "filename")String filename) throws Exception {
        FileDown fileDown = new FileDown();
        fileDown.deleteFile(group, filename);
        return new Result(true, StatusCode.OK, "删除成功");
    }



    /**
     * 文件更新
     * @param file
     * @param filename
     * @return
     * @throws Exception
     */
    @PatchMapping
    public Result updateFile(@RequestParam(value = "file")MultipartFile file,
                             @RequestParam(value = "filename")String filename) throws Exception {
        FastDFSFile fastDFSFile = new FastDFSFile(filename,file.getBytes(), StringUtils.getFilenameExtension(file.getOriginalFilename()));
        FileDown fileDown = new FileDown();
        String[] result = fileDown.updateFile(fastDFSFile);
        String filepath = "http://http://182.92.8.23:8080/"+result[0]+"/"+result[1];
        return new Result(true, StatusCode.OK, "上传成功",filepath);
    }

    /**
     * 所有相册查询
     * @return
     */
    @GetMapping
    public Result getAllAlbum(){
        List<Album> all = fileService.findAll();
        return new Result(true, StatusCode.OK, "查询所有相册成功",all);
    }

    @GetMapping("/{page}/{size}")
    public Result getAllAlbumByPage(@PathVariable(value = "page")Integer page,
                                    @PathVariable(value = "size")Integer size){
        PageInfo<Album> allByPage = fileService.findAllByPage(page, size);
        return new Result(true, StatusCode.OK, "分页查询所有相册成功",allByPage);
    }

    @DeleteMapping("/delete/{id}")
    public Result deleteAlbumById(@PathVariable(value = "id")Integer id){
        fileService.delete(id);
        return new Result(true, StatusCode.OK, "删除相册成功");
    }

    @GetMapping("/{id}")
    public Result findAlbumById(@PathVariable(value = "id")Integer id){
        Album album = fileService.findById(id);
        return new Result(true, StatusCode.OK, "查询相册成功",album);
    }

    @PostMapping("/save")
    public Result insertAlbum(@RequestBody Album album){
        fileService.insert(album);
        return new Result(true, StatusCode.OK, "保存相册成功");
    }

    @PatchMapping("/update")
    public Result updateAlbum(@RequestBody Album album){
        fileService.update(album);
        return new Result(true, StatusCode.OK, "更新相册成功");
    }



}
