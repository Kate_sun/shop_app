package com.sd.file.service;

import com.github.pagehelper.PageInfo;
import com.sd.good.pojo.Album;
import com.sd.good.pojo.Brand;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IFileService {
    /**
     * 模糊搜索
     */
    List<Album> findByExample(Album album);

    /**
     * 分页查询所有
     * @param page
     * @param size
     * @return
     */

    PageInfo<Album> findAllByPage(Integer page, Integer size);

    /**
     * 分页 模糊搜索
     * @param page
     * @param size
     * @return
     */
    PageInfo<Album> findExampleByPage(Album album , Integer page, Integer size);


    /**
     * 查询所有
     */
    List<Album> findAll();

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    Album findById(Integer id);

    /**
     * 添加单条
     * @param album
     */
    void insert(Album album);

    /**
     * 修改单条
     * @param album
     */
    void update(Album album);

    /**
     * 删除单条
     * @param id
     */
    void delete(Integer id);

}
