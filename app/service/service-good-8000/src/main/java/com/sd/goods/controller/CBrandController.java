package com.sd.goods.controller;

import com.github.pagehelper.PageInfo;
import com.sd.entity.Result;
import com.sd.entity.StatusCode;
import com.sd.good.pojo.Brand;
import com.sd.goods.service.impl.CBrandServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/brand")
@ResponseBody
@CrossOrigin
public class CBrandController {

    @Autowired
    private CBrandServiceImpl service;

    /**
     * 查找所有
     * @return
     */
    @GetMapping()
    public Result<List<Brand>> findAllBrand(){
        List<Brand> allBrand = service.findAll();
        return new Result<List<Brand>>(true, StatusCode.OK,"查询所有品牌信息成功",allBrand);
    }

    /**
     * 通过id查找
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Brand> findById(@PathVariable(value = "id") Integer id){
        Brand resultBrand = service.findById(id);
        return new Result<Brand>(true,StatusCode.OK,"查询品牌信息成功",resultBrand);
    }

    /**
     * 添加
     * @param brand
     * @return
     */
    @PostMapping()
    public Result insert(@RequestBody Brand brand){
        service.insert(brand);
        return new Result(true,StatusCode.OK,"添加品牌信息成功");
    }

    /**
     * 修改
     * @param id
     * @param brand
     * @return
     */
    @PatchMapping("/{id}")
    public Result update(@PathVariable(value = "id")Integer id , @RequestBody Brand brand){
        brand.setId(id);
        service.update(brand);
        return new Result(true,StatusCode.OK,"修改品牌信息成功");
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable(value = "id")Integer id){
        service.delete(id);
        return new Result(true,StatusCode.OK,"删除品牌信息成功");
    }

    /**
     * 搜索
     * @param brand
     * @return
     */
    @GetMapping("/search")
    public Result<List<Brand>> search(@RequestBody Brand brand){
        List<Brand> result = service.findByExample(brand);
        return new Result<List<Brand>>(true,StatusCode.OK,"搜索品牌信息成功",result);
    }

    /**
     * 分页查找所有
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/{page}/{size}")
    public Result<PageInfo<Brand>> findAllByPage(@PathVariable(value = "page")Integer page,
                                                 @PathVariable(value = "size")Integer size){
        PageInfo<Brand> allByPage = service.findAllByPage(page, size);
        return new Result<PageInfo<Brand>>(true,StatusCode.OK,"分页查询所有品牌信息成功",allByPage);
    }

    /**
     * 分页搜索
     * @param brand
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/search/{page}/{size}")
    public Result<PageInfo<Brand>> searchByPage(@RequestBody Brand brand,
                                                @PathVariable(value = "page")Integer page,
                                                @PathVariable(value = "size")Integer size){
        PageInfo<Brand> exampleByPage = service.findExampleByPage(brand, page, size);
        return new Result<PageInfo<Brand>>(true,StatusCode.OK,"分页搜索品牌信息成功",exampleByPage);
    }
}
