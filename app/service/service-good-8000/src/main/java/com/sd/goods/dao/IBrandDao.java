package com.sd.goods.dao;

import com.sd.good.pojo.Brand;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.common.Mapper;

@Component
public interface IBrandDao extends Mapper<Brand> {

}
