package com.sd.goods.service;

import com.github.pagehelper.PageInfo;
import com.sd.good.pojo.Brand;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IBrandService{
    /**
     * 模糊搜索
     */
    List<Brand> findByExample(Brand brand);

    /**
     * 分页查询所有
     * @param page
     * @param size
     * @return
     */

    PageInfo<Brand> findAllByPage(Integer page, Integer size);

    /**
     * 分页 模糊搜索
     * @param page
     * @param size
     * @return
     */
    PageInfo<Brand> findExampleByPage(Brand brand , Integer page, Integer size);


    /**
     * 查询所有
     */
    List<Brand> findAll();

    /**
     * 通过ID查询
     * @param id
     * @return
     */
    Brand findById(Integer id);

    /**
     * 添加单条
     * @param brand
     */
    void insert(Brand brand);

    /**
     * 修改单条
     * @param brand
     */
    void update(Brand brand);

    /**
     * 删除单条
     * @param id
     */
    void delete(Integer id);

}
