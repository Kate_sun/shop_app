package com.sd.goods.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sd.good.pojo.Brand;
import com.sd.goods.dao.IBrandDao;
import com.sd.goods.service.IBrandService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.StringUtil;

import java.util.List;

@Service
public class CBrandServiceImpl implements IBrandService {

    @Autowired
    private IBrandDao brandDao;

    /**
     * 模糊搜索
     */
    @Override
    public List<Brand> findByExample(Brand brand) {
        Example example = Example(brand);
        return brandDao.selectByExample(example);
    }

    /**
     *  模糊搜索模板
     * @param brand
     * @return
     */
    public Example Example(Brand brand) {
        Example example = new Example(Brand.class);
        Example.Criteria criteria = example.createCriteria();
        if (brand!=null)
            //如果名字不为空，则模糊搜索名字
            if (!StringUtil.isEmpty(brand.getName())){
                criteria.andLike("name","%"+brand.getName()+"%");
            }
        //如果首字母不为空，则搜索首字母
        if (!StringUtil.isEmpty(brand.getLetter())) {
            criteria.andEqualTo("letter", brand.getLetter());
        }
        return example;
    }

    /**
     * 分页搜索所有
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageInfo<Brand> findAllByPage(Integer page, Integer size) {
        PageHelper.startPage(page,size);
        List<Brand> brands = brandDao.selectAll();
        return new PageInfo<Brand>(brands);
    }

    /**
     * 分页模糊搜索
     * @param brand
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageInfo<Brand> findExampleByPage(Brand brand, Integer page, Integer size) {
        PageHelper.startPage(page,size);
        List<Brand> brands = brandDao.selectByExample(Example(brand));
        return new PageInfo<Brand>(brands);
    }

    /**
     * 查找所有
     * @return
     */
    @Override
    public List<Brand> findAll() {
        return brandDao.selectAll();
    }

    /**
     * 通过ID查找
     * @param id
     * @return
     */
    @Override
    public Brand findById(Integer id) {
        return brandDao.selectByPrimaryKey(id);
    }

    /**
     * 插入
     * @param brand
     */
    @Override
    public void insert(Brand brand) {
        brandDao.insertSelective(brand);
    }

    /**
     * 更新
     * @param brand
     */
    @Override
    public void update(Brand brand) {
        brandDao.updateByPrimaryKeySelective(brand);
    }

    /**
     * 删除
     * @param id
     */
    @Override
    public void delete(Integer id) {
        brandDao.deleteByPrimaryKey(id);
    }


}
