package com.sun.eureka_8100;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class eurekaSpringBoot {
    public static void main(String[] args) {
        SpringApplication.run(eurekaSpringBoot.class);
    }
}
