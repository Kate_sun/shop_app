import java.util.*;

class Salary{
    int BasicSalary;
    int allowance;
    int bonus;
    int totalSalary;
    Salary(){}
    Salary (int BasicSalary,int allowance,int bonus) {
        this.BasicSalary=BasicSalary;
        this.allowance=allowance;
        this.bonus=bonus;
        this.totalSalary=BasicSalary+allowance+bonus;
    }
}

class Employee  implements Comparable<Employee>{
    Salary salary;
    String name;
    int id;
    boolean sex;
    Department dapartment;
    Employee(){}

    Employee(int id,String name,boolean sex,Salary salary) {
        this.id=id;
        this.name=name;
        this.sex=sex;
        this.salary=salary;
    }
    int getTotalSalary(){return this.salary.totalSalary;}
    String getName() {return this.name;}
    int getId() {return id;}
    char getSex() {
        if (this.sex==true)
            return 'M';
        else return 'F';
    }

    @Override
    public int compareTo(Employee o) {
        return this.getTotalSalary() - o.getTotalSalary();
    }
}
class Department{
    int id;
    String name;
    int totalnum;
    Employee manager;
    Department(){}
    Department(int id,String name,int total){
        this.id=id;
        this.name=name;
        this.totalnum=total;
    }
    HashSet<Employee> Employees=new HashSet<Employee>();
    void add(Employee e) {this.Employees.add(e); this.totalnum++;}
    void setManager() {
        int SALARY[]=new int[100];
        int i=0;
        for(Employee e:this.Employees) {
            SALARY[i]=e.getTotalSalary();
            i++;
        }
        Arrays.sort(SALARY);
        for(Employee e:this.Employees) {
            if(e.getTotalSalary()==SALARY[SALARY.length-1])
                this.manager=e;
            else
                continue;
        }
    }
    int aver_Salary() {
        int aver=0;
        for (Employee e:this.Employees) {
            aver+=e.getTotalSalary()/this.totalnum;
        }
        return aver;
    }

    int getId() {return this.id;}
    String getName() {return this.name;}
    int getTotal() {return totalnum;}
    float sexFratio() {
        float m=0,f=0;
        for(Employee e:this.Employees) {
            if (e.getSex()=='M')
                m+=1.0;
            else f+=1.0;
        }
        return m/f;
    }
}
public class b {
    static String names[]= {"Mary","Dorothy","Helen","Sio","Ruth","Rina","Virginia","Elizabeth","Frances","Anna","Betty",
            "Harold","Marie","Maria","Alice","Florence","Irene","Lillian","Louise","Rose","Catherine","Mark",
            "Martha","Jerry","James","Steve","Ruby","Jobs","David","Jessica","Joy","Edith","Jean","Hazel",
            "Tom","Jack","Jae","Jeno","John","Clara","Katherine","Emma","Barbara","Jane",
            "Ann","Sarah","Elsie","Lua","Jones","Elly","Julia","Wendy","Bertha","Eva",
            "Angela","Hua Li","Green","Pearl","Laura","Vivian","Anne","Ida","Kathryn","Leo","Norma","Lorraine",
            "Louris","Kimi","Marian","Christopher","Vera","Alan","Violet","Gloria","Lauv","Alma","Maxine","Stella",
            "Brian","Jessie","Sylvia","Genevieve","Minnie","Ella","Anne","Lily","Kassy","Ellen","Anson","Pauline","Grace","Lois",
            "Lena","Lucy","Nancy","Mattie","Max","Lucy","Mae","Katty"};
    //创建5个部门
    static Department departments[]=new Department[5];
    static Department Accounting=new Department(1,"Accounting",0);
    static Department Sale=new Department(2,"Sale",0);
    static Department Publicity=new Department(3,"Publicity",0);
    static Department Personnel=new Department(4,"Personnel",0);
    static Department Organization=new Department(5,"Organization",0);
    static HashSet<Employee> Employees=new HashSet<Employee>();


    static void welcome() {
        Scanner sin=new Scanner(System.in);
        System.out.println("选择你想要进行的操作并输入:");
        System.out.print("1.显示某一部门员工信息"+"\n"+"2.总业绩排行"+"\n"+"3.查询员工业绩情况"+"\n"+"4.部门平均业绩排行"+"\n"+"5.部门男女比例"+"\n"+"6.退出系统"+"\n");
        int n;
        String dname;
        String ename;
        n=sin.nextInt();

        switch(n) {
            case 1:
                System.out.println("请输入你想要查询的部门（Accounting/Sale/Publicity/Personnel/Organization）：");
                dname=sin.next();
                int r=1;
                for (Department d:departments) {
                    if (d.getName()==(dname)) {
                        System.out.println("部门总人数："+d.totalnum);
                        System.out.print("rank"+"\t"+"name\t"+"\t"+"id\t"+"\t"+"sex\t"+"\t"+"salary"+"\n");
                        for (Employee e:d.Employees) {
                            System.out.print(r+"\t");
                            if (e.getName().length()<8) {
                                System.out.print(e.getName()+"\t"+"\t");
                            }
                            else if(e.getName().length()>=8) {System.out.print(e.getName()+"\t");}
                            System.out.println(e.getId()+"\t"+"\t"+e.getSex()+"\t"+"\t"+e.getTotalSalary()+"\n");
                            r+=1;
                        }
                    }
                }
                System.out.println("本次使用已经完成，请再次选择功能或退出："+"\n");
                welcome();
                break;
            case 2:
                int R=1;
                List list = new ArrayList();
                for (Employee e:Employees) {
                    list.add(e);
                }
                Collections.sort(list);
                for(Object i : list){
                    System.out.println(i);
                }
                    System.out.print("rank"+"\t"+"name\t"+"\t"+"id\t"+"\t"+"sex\t"+"\t"+"salary"+"\n");
                System.out.println("本次使用已经完成，请再次选择功能或退出："+"\n");
                welcome();
                break;
            case 3:
                System.out.println("请输入您要查询的员工名字："+"\n");
                ename=sin.next();
                for (Employee e:Employees) {
                    if (e.getName()==ename) {
                        System.out.println("basic salary:"+e.salary.BasicSalary+"\n"+"allowance:"+e.salary.allowance+"\n"+"bonus:"+e.salary.bonus+"\n"+"total salary:"+e.salary.totalSalary+"\n");
                    }
                }
                System.out.println("本次使用已经完成，请再次选择功能或退出："+"\n");
                welcome();
                break;
            case 4:
                System.out.print("rank"+"\t"+"name"+"\t"+"\t"+"equal salary"+"\n");
                for (int i=0;i<5;i++) {
                    System.out.print(i+1+"\t");
                    if (departments[i].getName().length()<8) {
                        System.out.print(departments[i].getName()+"\t"+"\t");
                    }
                    else System.out.print(departments[i].getName()+"\t");
                    System.out.println(departments[i].aver_Salary()+"\n");
                }
                System.out.println("本次使用已经完成，请再次选择功能或退出："+"\n");
                welcome();
                break;
            case 5:
                System.out.print("name"+"\t"+"\t"+"M/F"+"\n");
                for (int i=0;i<5;i++) {
                    if (departments[i].getName().length()<8) {System.out.print(departments[i].getName()+"\t"+"\t");}
                    else System.out.print(departments[i].getName()+"\t");
                    System.out.print(departments[i].sexFratio()+"\n");
                }
                System.out.println("本次使用已经完成，请再次选择功能或退出：");
                welcome();
                break;
            case 6:
                System.out.print("功能关闭");
                System.exit(1);
        }
    }

    public static void main(String args[]) {
        int length=100;
        int BasicalSalary,allowance,bouns;
        boolean sex;
        String name;
        int flag;
        for (int i=0;i<100;i++) {
            BasicalSalary=(int)(Math.random()*3000)+2000;
            allowance=(int)(Math.random()*5000)+3000;
            bouns=(int)(Math.random()*2000)+1500;
            Salary tempSalary=new Salary(BasicalSalary,allowance,bouns);
            if ((int)(Math.random()*2)==1)
                sex=true;
            else sex=false;
            flag=(int)(Math.random()*length);
            name=names[flag];
            names[flag]=names[length-1];
            length--;
            Employee staff=new Employee(i+1,name,sex,tempSalary);
            Employees.add(staff);
        }
        //将员工放入部门中
        int j=1;
        for (Employee e:Employees) {
            j=j%5;
            switch(j) {
                case 0:
                    Accounting.add(e);
                    e.dapartment=Accounting;
                    break;
                case 1:
                    Sale.add(e);
                    e.dapartment=Sale;
                    break;
                case 2:
                    Publicity.add(e);
                    e.dapartment=Publicity;
                    break;
                case 3:
                    Personnel.add(e);
                    e.dapartment=Personnel;
                    break;
                case 4:
                    Organization.add(e);
                    e.dapartment=Organization;
                    break;
            }
            j+=1;
        }

        departments[0]=Accounting; departments[1]=Sale; departments[2]=Publicity; departments[3]=Personnel; departments[4]=Organization;
        for (int i=4;i>0;i--) {
            for (j=0;j<i;j++) {
                if (departments[j].aver_Salary()<departments[j+1].aver_Salary()) {
                    Department tempDepartment=departments[j];
                    departments[j]=departments[j+1];
                    departments[j+1]=tempDepartment;
                }
            }
        }
        welcome();
    }
}
