package com.sd.good.pojo;

import java.io.Serializable;
import java.util.Arrays;

public class FastDFSFile implements Serializable {
    //文件名字
    private String name;
    //文件内容
    private byte[] content;
    //文件扩展名
    private String ext;
    //文件MD5摘要
    private String md5;
    //作者名字
    private String autuor;

    public FastDFSFile(String name,byte[] content,String ext) {
        this.name = name;
        this.content = content;
        this.ext = ext;
    }

    @Override
    public String toString() {
        return "FastDFSFile{" +
                "name='" + name + '\'' +
                ", content=" + Arrays.toString(content) +
                ", ext='" + ext + '\'' +
                ", md5='" + md5 + '\'' +
                ", autuor='" + autuor + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getAutuor() {
        return autuor;
    }

    public void setAutuor(String autuor) {
        this.autuor = autuor;
    }
}
